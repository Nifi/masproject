#!/bin/bash

OUTPUT="./plot3.tex"


echo "\begin{tikzpicture}" > $OUTPUT
echo "\begin{axis}[" >> $OUTPUT
echo "    title={Experiment 3}," >> $OUTPUT
echo "    xlabel={Packet arrival rate}," >> $OUTPUT
echo "    ylabel={Packets picked up / minute}," >> $OUTPUT
echo "    xmin=0.002, xmax=0.011," >> $OUTPUT
echo "    ymin=0, ymax=40," >> $OUTPUT
echo "    xtick={.002  , .003  , .004  , .005  , .006  , .007  , .008  , .009  , .010 , .011}," >> $OUTPUT
echo "    ytick={0,10,20,30,40}," >> $OUTPUT
echo "    legend pos=north west," >> $OUTPUT
echo "    ymajorgrids=true," >> $OUTPUT
echo "    grid style=dashed," >> $OUTPUT
echo "]" >> $OUTPUT
 
cat ./plot13.tex >> $OUTPUT
cat ./plot23.tex >> $OUTPUT
cat ./plot33.tex >> $OUTPUT
echo "\legend{CNET, CNET with negotiations, Gradient Field}" >> $OUTPUT
 
echo "\end{axis}" >> $OUTPUT
echo "\end{tikzpicture}" >> $OUTPUT
