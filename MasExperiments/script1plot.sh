#!/bin/bash

OUTPUT=$1


echo "\addplot[" > $OUTPUT
echo "    color=$2," >> $OUTPUT
echo "    mark=square," >> $OUTPUT
echo "    ]" >> $OUTPUT
echo "    coordinates {" >> $OUTPUT


LINE=""

for s in {120..129}
do

    for p in {2..11}
	do

        file="$s-$p.txt"

        VIOLATED=`cat $file | grep "too late" | wc -l`
        #RESPECTED=`cat $file | grep "in time" | wc -l`
        TOTAL=`cat $file | grep "in time" | wc -l`

ASDF=`python <<END
ASDF = ($TOTAL-$VIOLATED)/float($TOTAL)
print("%.2f" % round(ASDF*100,2))
END`

        var="I$s$p"
        eval $var=\$ASDF

    done

done


for p in {2..11}
do
    sum="0"
    for s in {120..129}
    do
        var="I$s$p"
        num=${!var}
        sum=$(python -c "print($sum+$num)")
    done

AVERAGE=`python <<END
AVERAGE = $sum/float(10)
print("%.2f" % round(AVERAGE,2))
END`

p2=`python <<END
p2 = 0.001*$p
print("%.3f" % round(p2,3))
END`

    LINE="$LINE($p2,$AVERAGE)"
	
done


echo "$LINE" >> $OUTPUT



echo "    };" >> $OUTPUT

