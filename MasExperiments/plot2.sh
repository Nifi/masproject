#!/bin/bash

OUTPUT="./plot2.tex"


echo "\begin{tikzpicture}" > $OUTPUT
echo "\begin{axis}[" >> $OUTPUT
echo "    title={Experiment 2}," >> $OUTPUT
echo "    xlabel={Packet arrival rate}," >> $OUTPUT
echo "    ylabel={Average battery consumption / drone}," >> $OUTPUT
echo "    xmin=0.002, xmax=0.011," >> $OUTPUT
echo "    ymin=0, ymax=20," >> $OUTPUT
echo "    xtick={.002  , .003  , .004  , .005  , .006  , .007  , .008  , .009  , .010 , .011}," >> $OUTPUT
echo "    ytick={0,5,10,15,20}," >> $OUTPUT
echo "    legend pos=south east," >> $OUTPUT
echo "    ymajorgrids=true," >> $OUTPUT
echo "    grid style=dashed," >> $OUTPUT
echo "]" >> $OUTPUT
 
cat ./plot12.tex >> $OUTPUT
cat ./plot22.tex >> $OUTPUT
cat ./plot32.tex >> $OUTPUT
echo "\legend{CNET, CNET with negotiations, Gradient Field}" >> $OUTPUT
 
echo "\end{axis}" >> $OUTPUT
echo "\end{tikzpicture}" >> $OUTPUT
