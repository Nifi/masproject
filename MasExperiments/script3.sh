#!/bin/bash

OUTPUT=$1

echo "\begin{table}[H]" > $OUTPUT
echo "\centering" >> $OUTPUT
echo "\caption{$2}" >> $OUTPUT
echo "\label{$3}" >> $OUTPUT
echo "\begin{tabular}{|l|l|l|l|l|l|l|l|l|l|l|}" >> $OUTPUT
echo "\hline" >> $OUTPUT
echo "prob & .002  & .003  & .004  & .005  & .006  & .007  & .008  & .009  & .010 & .011  \\\ \hline" >> $OUTPUT


for s in {120..129}
do

	for p in {2..11}
	do

        file="$s-$p.txt"
        
        PICKEDUP=`cat $file | grep "picked up" | wc -l`

RATE=`python <<END
RATE = $PICKEDUP/float(10)
print("%.2f" % round(RATE,2))
END`

        var="I$s$p"
        eval $var=\$RATE

	done

done

LINE="average "

for p in {2..11}
do
    sum="0"
    for s in {120..129}
    do
        var="I$s$p"
        num=${!var}
        sum=$(python -c "print($sum+$num)")
    done

AVERAGE=`python <<END
AVERAGE = $sum/float(10)
print("%.2f" % round(AVERAGE,2))
END`

    LINE="$LINE & $AVERAGE"

    sum="0"
    for s in {120..129}
    do
        var="I$s$p"
        num=${!var}
        sum=$(python -c "print($sum+($num-$AVERAGE)*($num-$AVERAGE))")
    done

deviation=`python <<END
import math
print(math.sqrt($sum/float(10)))
END`

	deviation=$(python -c "print(\"%.2f\" % round($deviation,2))")

	var="D$p"
    eval $var=\$deviation

done

LINE="$LINE \\\ \hline"
echo "$LINE" >> $OUTPUT


LINE="deviation "

for p in {2..11}
do
  
	var="D$p"
	dev=${!var}

	LINE="$LINE & $dev"
	
done

LINE="$LINE \\\ \hline"
echo "$LINE" >> $OUTPUT


echo "\end{tabular}" >> $OUTPUT
echo "\end{table}" >> $OUTPUT
