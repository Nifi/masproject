#!/bin/bash

OUTPUT=$1


echo "\addplot[" > $OUTPUT
echo "    color=$2," >> $OUTPUT
echo "    mark=square," >> $OUTPUT
echo "    ]" >> $OUTPUT
echo "    coordinates {" >> $OUTPUT


LINE=""

for s in {120..129}
do

	for p in {2..11}
	do

        file="$s-$p.txt"
        
        PICKEDUP=`cat $file | grep "picked up" | wc -l`

RATE=`python <<END
RATE = $PICKEDUP/float(10)
print("%.2f" % round(RATE,2))
END`

        var="I$s$p"
        eval $var=\$RATE

	done

done


for p in {2..11}
do
    sum="0"
    for s in {120..129}
    do
        var="I$s$p"
        num=${!var}
        sum=$(python -c "print($sum+$num)")
    done

AVERAGE=`python <<END
AVERAGE = $sum/float(10)
print("%.2f" % round(AVERAGE,2))
END`

p2=`python <<END
p2 = 0.001*$p
print("%.3f" % round(p2,3))
END`

    LINE="$LINE($p2,$AVERAGE)"
	
done


echo "$LINE" >> $OUTPUT



echo "    };" >> $OUTPUT

