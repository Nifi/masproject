
cd EXP1
./../script1.sh "../tables/table11.tex" "Experiment 1 with CNET" "table11"
./../script1plot.sh "../plots/plot11.tex" "blue" "CNET"
./../script2.sh "../tables/table12.tex" "Experiment 2 with CNET" "table12"
./../script2plot.sh "../plots/plot12.tex" "blue" "CNET"
./../script3.sh "../tables/table13.tex" "Experiment 3 with CNET" "table13"
./../script3plot.sh "../plots/plot13.tex" "blue" "CNET"
cd ..

cd EXP2
./../script1.sh "../tables/table21.tex" "Experiment 1 with CNET with negotiations" "table21"
./../script1plot.sh "../plots/plot21.tex" "green" "CNET with negotiations"
./../script2.sh "../tables/table22.tex" "Experiment 2 with CNET with negotiations" "table22"
./../script2plot.sh "../plots/plot22.tex" "green" "CNET with negotiations"
./../script3.sh "../tables/table23.tex" "Experiment 3 with CNET with negotiations" "table23"
./../script3plot.sh "../plots/plot23.tex" "green" "CNET with negotiations"
cd ..

cd EXP3
./../script1.sh "../tables/table31.tex" "Experiment 1 with Gradient Field" "table31"
./../script1plot.sh "../plots/plot31.tex" "red" "Gradient Field"
./../script2.sh "../tables/table32.tex" "Experiment 2 with Gradient Field" "table32"
./../script2plot.sh "../plots/plot32.tex" "red" "Gradient Field"
./../script3.sh "../tables/table33.tex" "Experiment 3 with Gradient Field" "table33"
./../script3plot.sh "../plots/plot33.tex" "red" "Gradient Field"
cd ..

cd plots
./../plot1.sh
./../plot2.sh
./../plot3.sh
cd ..
