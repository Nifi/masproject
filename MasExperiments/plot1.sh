#!/bin/bash

OUTPUT="./plot1.tex"


echo "\begin{tikzpicture}" > $OUTPUT
echo "\begin{axis}[" >> $OUTPUT
echo "    title={Experiment 1}," >> $OUTPUT
echo "    xlabel={Packet arrival rate}," >> $OUTPUT
echo "    ylabel={Satisfaction rate}," >> $OUTPUT
echo "    xmin=0.002, xmax=0.011," >> $OUTPUT
echo "    ymin=0, ymax=100," >> $OUTPUT
echo "    xtick={.002  , .003  , .004  , .005  , .006  , .007  , .008  , .009  , .010 , .011}," >> $OUTPUT
echo "    ytick={0,20,40,60,80,100}," >> $OUTPUT
echo "    legend pos=south west," >> $OUTPUT
echo "    ymajorgrids=true," >> $OUTPUT
echo "    grid style=dashed," >> $OUTPUT
echo "]" >> $OUTPUT
 
cat ./plot11.tex >> $OUTPUT
cat ./plot21.tex >> $OUTPUT
cat ./plot31.tex >> $OUTPUT
echo "\legend{CNET, CNET with negotiations, Gradient Field}" >> $OUTPUT

echo "\end{axis}" >> $OUTPUT
echo "\end{tikzpicture}" >> $OUTPUT
