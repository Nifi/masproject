\documentclass[a4paper,10pt]{article}

\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage[colorlinks, linkcolor=black, citecolor=black, urlcolor=black]{hyperref}
\usepackage{geometry}
\geometry{tmargin=3cm, bmargin=2.2cm, lmargin=2.2cm, rmargin=2cm}
\usepackage{float}
\usepackage{pgfplots}
\pgfplotsset{width=16cm,compat=1.9}
\usepackage{soul}



\begin{document}

\input{titlepage}

\tableofcontents

\newpage

\section{Introduction}
In this paper we will compare the scalability of different algorithms for allocating tasks in a pick-up and delivery problem. We will study the following algorithms:
\begin{itemize}
    \item CNET
    \item CNET with negotiations
    \item Gradient Field
\end{itemize}

When comparing and researching the algorithms mentioned above, we looked at the following variables:
\begin{itemize}
    \item Number of time window violations
    \item Average battery consumption per drone
    \item Number of tasks completed per unit of time
\end{itemize}

In order to conduct the experiments, we used the following working example:
\newline
A company wants to pick up packages at customers and transport them to a central warehouse using drones. In our example we will have three warehouses and 10 drones. A drone can hold up to three packages. Packages can be added to the environment in a dynamical way. The operating environment is a rectangular area measuring 1.3 by 1.3 kilometers. When there is a package available for pick-up, it broadcasts a signal. A package knows to which warehouse it needs to be transported. Packages only have a time window for pick up. Once a packet is picked up it is considered to be in the system, therefore delivery time is not relevant. There are 15 charging stations located in the operating environment. A charging station has a capacity of 1 drone.

\subsection{Objectives}
\begin{itemize}
\item Comparing the scalability of CNET, CNET with negotiations and Gradient Field
\item Implementing the algorithms to run the experiments
\item Writing a scientific paper about the findings
\end{itemize}

\subsection{Questions}
\begin{itemize}
    \item When looking at the number of time window violations, how do the three algorithms compare to each other?
    \item When looking at the average battery consumption per drone, how do the three algorithms compare to each other?
    \item When looking at the number of tasks completed per unit of time, how do the three algorithms compare to each other?
\end{itemize}


\subsection{Hypotheses}
\begin{itemize}
\item When looking at the number of time window violations, CNET performs worse than CNET with negotiations.
\item When looking at the number of time window violations, CNET performs worse than Gradient Field.
\item When looking at the number of time window violations, CNET with negotiations performs worse than Gradient Field.
\newline
\item When looking at the average battery consumption per drone, CNET performs worse than CNET with negotiations.
\item When looking at the average battery consumption per drone, Gradient Field performs worse than CNET.
\item When looking at the average battery consumption per drone, Gradient Field performs worse than CNET with negotiations.
\newline
\item When looking at the number of tasks completed per unit of time, CNET performs worse than CNET with negotiations.
\item When looking at the number of tasks completed per unit of time, CNET performs worse than Gradient Field.
\item When looking at the number of tasks completed per unit of time, CNET with negotiations performs worse than Gradient Field.
\end{itemize}


\section{Theory}
In this section we will first discuss the most important aspects of each algorithm. Then we will compare the three algorithms with scalability in mind. We assume that the reader is familiar with CNET (with negotiations) and Gradient Field. This section is the theoretical foundation for the conducted experiments.

\subsection{CNET}

There are two main roles in the contract net protocol: the initiator and the participants. In the working example, a packet acts like the initiator and the drones are the participants. When a packet is available it sends out a cfp (call for proposals) message. Since packets have a limited communication range, only the drones nearby the packet will receive this message. Drones reply by either a refuse message or a propose message. This happens within a single tick. A drone replies with a refuse message when it has already more then a certain amount of task to execute (in the working example this amount is equal to 3). When this is not the case, a drone will send a propose message. A drone calculates its offer value and sends this to the packet with the propose message. The packet will choose the best drone. Calculating the offer value and choosing the best drone is done according to the objective function described below. The best drone will receive an accept\_proposal message. The other drones will receive a reject\_proposal message. A cfp message is a broadcast message, all other messages are unicast.

\paragraph{Objective function}
The following objective function is used for selection of a drone by a packet and selecting the next task to be performed by a drone. The smaller the value of the objective function, the better.
\begin{equation}
    timeTillTimeWindowViolation/timeWindow + distance/maxDistance + cargo/cargoCapacity
\end{equation}

Explanation of the variables in the objective function:
\begin{itemize}
    \item \textbf{timeTillTimeWindowViolation} \newline
    The amount of time until the time window is violated.
    
    \item \textbf{timeWindow} \newline
    The amount of time in which a packet needs to be picked up. This is the pick up time window.
    
    \item \textbf{distance} \newline
    The distance between the drone and the packet.
    
    \item \textbf{maxDistance} \newline
    The maximum possible distance in the environment. In the working example this is a rectangular shaped area so the maximum distance is the diagonal of the rectangle.
    
    \item \textbf{cargo} \newline
    The number of packets in cargo.
    
    \item \textbf{cargoCapacity} \newline
    The capacity of the cargo, this is the maximum number of packets a drone can carry.
\end{itemize}

Since delivery time is not considered, there needs to be a difference between packets to pick up and packets to deliver. Time window violations have to be taken into account for packets to pick up but are not relevant for packets to deliver. To be able to use the same objective function for both types of packets timeTillTimeWindowViolation is given the value of timeWindow. This means that this fraction has always a value of 1. Now priority is given to packets to be picked up but distance and cargo or still taken into account. \newline
To fine-tune the objective function, each term can be multiplied with a weight in order to give the different terms relative importance. In the working example, the objective function is used with al terms having weight 1.

\subsection{CNET with negotiations}

When negotiating, two drones will try to improve the total situation of the system, not their own situation.
Drones have a list of packets to pick up. In our working example this list can be up to three packets long. A drone broadcasts a negotiate\_me message when there are items in its pick-up list and it is not charging or already negotiating.
When a drone receives a negotiate\_me message and it is available for negotiation it replies with an accept\_negotiation message.
Negotiation is based upon the items in the pick up list of both drones. At the start of a negotiation, both drones exchange their pick up lists. Each drone calculates the power set of all items in both its pick up list and the other drone's pick up list. For each item in the power set the objective function is calculated. This is the same function as used for selection of a drone by a packet in CNET. If the objective function needs to be calculated for multiple packets, the objective function is calculated for each packet and the values are added together. Both drones also calculate the value of the objective function for the items currently in their pick up list. The smaller the value of the objective function, the better. After the calculation is done, the items of the power set are ranked according to the value of the objective function, the smallest first. This list is called the list of possible proposals.
The first proposal that each drone makes is the first item in its list of possible proposals. The second proposal is the second item, ... This means that a drone proposes in the order that is the most interesting for itself.
This proceeds until a satisfying deal is found (the negotiation ends succesfully) or there are no more deals left to propose (the negotiation is closed).
At each deal proposed, both drones exchange their proposal, the value of the objective function for that proposal for itself and the value of the objective function for the items currently in its pick up list. A deal is accepted when the sum of the values of the objective function for the proposal is less than the sum of the values of the objective function for the items currently in their pick up lists. So a negotiation ends succesfully only when the total situation can be improved. 
This means that the packets in the pick up lists of both drones are redistributed among each other in such a way that the sum of the objective functions of both drones has decreased.

\subsection{Gradient Field}
Packets emit positive fields, drones emit negative fields. Drones follow the gradient of these fields to deliver packets. When a drone is nearby a packet it flies right to the packet when it has space available in its cargo without taking the field into account. When a drone is nearby a warehouse it checks whether it has packets in its cargo to be delivered at that warehouse, if so, it flies right to that warehouse without taking the field into account. Nearby is defined as whitin a certain range. This is continuously checked by the drone.
When the cargo space of a drone is full, the drone does not take the emitted fields of the packets to pick up into account. It calculates the gradient based on the packets in its cargo to deliver and the other drones around. This means that a drone is attracted by a warehouse, although the warehouse does not emit a field. When there are multiple packets for the same warehouse, the "field" of the warehouse becomes stronger. 


\subsection{Comparison}
In this section CNET, CNET with negotiations and Gradient Field are compared to each other and the hypotheses stated above are clarified.
\newline
When comparing CNET with CNET with negotiations we expect CNET with negotiations to outperform CNET since drones continuously try to improve the total situation of the system with negotiations. Of course, these negotiations introduce communication and calculating overhead. We expect this overhead to be rather minimal compared to the improvement it brings. We expect to see this improvement when we look at the number of time window violations, the average battery consumption per drone and the number of tasks completed per unit of time.
\newline
Gradient field is more reactive then CNET and CNET with negotiations. The environment in the working example changes fast and fast reaction to this change is needed. Gradient field handles this reaction to the environment in a fast, simple and intuitive way. In CNET and CNET with negotiations there is a lot more communication (compared to Gradient Field where there is none) and more calculation going on. This introduces overhead. Therefore we expect CNET and CNET with negotiations to be less reactive and slower. Since CNET and CNET with negotiations react less fast to changes in the environment we expect Gradient Field to outperform CNET and CNET with negotiations when we look a the number of time window violations and the number of tasks completed per unit of time. Although, when we look at the average battery consumption we expect this to be greater in Gradient Field because packet assignment is delayed until pick up. Multiple drones can be attracted by a same packet. This means that there is no guarantee that following this field will lead to a pick up. Drones can thus go to a packet that needs to be picked up unnecessarily, while possibly having packets in cargo, and thus losing valuable battery energy. Since the emitted fields continuously change, the destination of a drone can change rapidly. In CNET and CNET with negotiations a drone only flies to a packet when it is claimed by that packet. It will also decide more easily to deliver a packet, when that is interesting, and thus reduce the load. Therefore we think that CNET and CNET with negotiations will outperform Gradient Field when looking at the average battery consumption per drone.

\section{Multi-agent system design}

In this section each element of this multi-agent system design is discussed briefly. Some design decisions vary depending on the algorithm that is used. Therefore each element is discussed in the light of CNET and CNET with negotiations and the major differences in design are discussed for Gradient Field. \newline

When working with drones, safety and fast reaction is an important aspect. It is important to keep the power consumption and mass of the drone as low as possible. The mass has a direct impact on the power consumption and on the cargo that can be transported. The lesser the mass of the drone, the more the cargo mass can be. Of course, we want to transport as much as possible. When operating in the open environment, drones cannot land where they want. If we lose time calculating paths, making plans or communicating with other drones and packets, we lose valuable energy. Both to limited power and mass, the available processing power will be rather small. Therefore we cannot afford to use complex and time consuming algorithms. This is the main reason to choose for algorithms like CNET, CNET with negotiations or Gradient Field.

\subsection{CNET and CNET with negotiations}

\subsubsection{Drone}
A drone flies randomly around until it is chosen by a packet or is assigned one or more packets due to a successful negotiation. At that moment the drone flies as fast as possible to the best packet in its pick up list.

\paragraph{Avoiding collisions}
A drone will always follow the shortest path, this is a straight line between its current position and its destination. If needed, a drone will deviate from its path in order to avoid collisions.
To avoid collisions, a drone continuously broadcasts avoid\_me messages to other drones in a close range. When a drone receives an avoid\_me message it will calculate the distance between the sending drone and the path that the receiving drone is currently following. If this distance is smaller than a certain threshold, the receiving drone will generate a new random position closeby. It is checked that this new position will not cause a collision and that it lies within the field. Otherwise a new random position closeby is generated until the constraints above are satisfied. After a drone reached the random position it proceeds its way to its destination. \\
Collisions are also specially avoided nearby warehouses and charging stations, where the chance of collisions between drones that go to the same warehouse or charging station is high.
When a drone is going to a warehouse or charging station, and is approaching that destination, the drone checks if there is another drone nearby closer to that destination. If that is the case, the drone waits there until there is no other drone anymore nearby the warehouse or charging station.

\paragraph{Communication}
A drone has a limited communication range. It changes its communication range depending on the kind of message to send. We encountered a problem using the CommDevice of RinSim: the sending range could not be adapted during runtime. We modified the CommDevice class so we can now set the range during execution. The communication range is kept small for avoid-me messages since this is only a concern very close to the drone. We also assume that if a packet can reach a drone, the drone can reach the packet and vice versa. This is also true for the communication between drones. This is to avoid overhead with dealing with communication boundary cases (e.g. drones in range at start of negotiation but going out of range during negotiation).

\paragraph{Battery capacity}
A drone is battery powered. Its battery can be charged at a charging station. The power consumption of a drone depends on the number of packets that it is carrying. The more packets it carries, the more power it consumes. Per unit of distance travelled, a specific amount of power is consumed. Every time the destination of a drone changes it checks whether it can reach the destination and fly safely back to a charging station.

\subsubsection{Packet}
A packet has a pick up time window. As time goes by, the chance of violating the time window grows. The communication range is extended after some time to increase the possibility to contact a drone nearby.
Packets are inserted into the system with a fixed probability.

\subsubsection{Warehouse}
Packets are delivered at warehouses. These warehouses have a specific location. These warehouses have further no function.

\subsubsection{Charging station}
Drones are charged at charging stations. These charging stations have a specific location. A charging station can only charge one drone at a time. When a drone arrives at a charging station that is currently occupied, the drone waits nearby until the charging station is free again. Charging is done relatively fast so it is ok for a drone to wait until the station is free.

\subsection{Gradient Field}

\subsubsection{Drone}
A drone flies around and follows the gradient of the emitted fields.

\paragraph{Avoiding collisions}
Since drones emit repulsive fields, this is also used for collision avoidance.

\paragraph{Battery capacity}
A drone continuously ensures that it can safely fly back to the nearest charging station.

\subsubsection{Packet}
As time goes by and the chance of violating the time window grows, the field that is emitted by a packet increases.

\section{Experiments}

\begin{figure}[h]
    \caption{Demo example}
    \includegraphics[scale=0.4]{demo.png}
    \centering
    \label{fig-demo}
\end{figure}

In RinSim, we used the following color code:
\begin{itemize}
    \item green: drone
    \item blue: package
    \item red: warehouse
    \item purple: charging station
\end{itemize}


\break
We have conducted the following experiments:

\subsection{Experiment 1: Comparing the number of time window violations}
The goal of the first experiment is to investigate how CNET, CNET with negotiations and Gradient Field compare to each other when looking at the number of time window violations when the packet arrival rate (this is the rate at which packets are inserted into the system) is varied.

\subsubsection{Setup}
The experiment is conducted in the following situation: 10 drones, 15 initial packets, 15 charging stations, 3 warehouses, a plane size of 1300m by 1300m, a drone capacity of 3 and every package has a pickup time window of 4000000 starting from the moment it is inserted into the system. The simulation is ran for 10 minutes. The packet arrival rate is varied between .002 and .011 in steps of .001. For every packet arrival rate we conducted 10 experiments each time using a different random seed.
In the results, the satisfaction rate is shown. The satisfaction rate is defined as the percentage of the number of pick up time window violations on the total number of packets.

\paragraph{Variables}
Independent variables:
\begin{itemize}
    \item packet arrival rate
    \item random seed
\end{itemize}

Dependent variables: 
\begin{itemize}
    \item number of time window violations
\end{itemize}

\subsubsection{Results}


\input{./MasExperiments/tables/table11.tex}

\input{./MasExperiments/tables/table21.tex}

\input{./MasExperiments/tables/table31.tex}

\input{./MasExperiments/plots/plot1.tex}


\subsubsection{Analysis}

When comparing both CNET and CNET with negotiations with Gradient Field it is clearly visible that Gradient Field field performs better due to its more reactive nature. Gradient Field reacts faster to changes in the environment and loses less time communicating. Therefore we can confirm the following hypotheses:
\begin{itemize}
    \item When looking at the number of time window violations, CNET performs worse than Gradient Field.
    \item When looking at the number of time window violations, CNET with negotiations performs worse than Gradient Field.
\end{itemize}

We expected CNET to perform worse than CNET with negotiations, this is not what is seen in the results. The opposite seems to be true, although there is not a big difference. When the packet arrival rate increases, each drone will claim as much packets as possible. This means that the negotiation set increases too. There is no guarantee that a negotiation will have a positive outcome. So drones can lose a lot of time calculating their negotiation sets and communicating without the guarantee of any result. Unfortunately, we see that the overhead that is introduced by negotiations is greater than the improvement it brings. This means that the following hypotheses is rejected:
\begin{itemize}
    \item When looking at the number of time window violations, CNET performs worse than CNET with negotiations.
\end{itemize}
See also section 2.4.


\subsection{Experiment 2: Comparing the average battery consumption per drone}
The goal of the second experiment is to investigate how CNET, CNET with negotiations and Gradient Field compare to each other when looking at the average battery consumption per drone when the packet arrival rate is varied.

\subsubsection{Setup}
The experiment is conducted in the following situation: 10 drones, 15 initial packets, 15 charging stations, 3 warehouses, a plane size of 1300m by 1300m, a drone capacity of 3 and every package has a pickup time window of 4000000 starting from the moment it is inserted into the system. The simulation is ran for 10 minutes. The packet arrival rate is varied between .002 and .011 in steps of .001. For every packet arrival rate we conducted 10 experiments each time using a different random seed. For each packet arrival rate, the average and standard deviation is calculated of the average battery consumption per drone. The battery consumption is shown as the number of full batteries needed during the simulations.

\paragraph{Variables}
Independent variables:
\begin{itemize}
    \item packet arrival rate
    \item random seed
\end{itemize}

Dependent variables: 
\begin{itemize}
    \item average battery consumption per drone
\end{itemize}


\subsubsection{Results}

\input{./MasExperiments/tables/table12.tex}

\input{./MasExperiments/tables/table22.tex}

\input{./MasExperiments/tables/table32.tex}

\input{./MasExperiments/plots/plot2.tex}


\subsubsection{Analysis}
This experiment looks at the average battery consumption per drone, and not at the battery consumption differences between drones, and thus nothing can be concluded about that.
The average battery consumption per drone is very similar for CNET with and without negotiations, with only very small differences that further seem to be insignificant.
With increasing packet arrival rate the battery consumption grows, because as there are more packets to pick up, the number of packets a drone will have in cargo will increase. And since the battery consumption depends on the cargo, the average battery consumption will increase.
The battery consumption still increases with high packet arrival rates, but much less, because most of the drones will have a full cargo most of the time. \\
When comparing with Gradient field, it is clear that Gradient field has a higher battery consumption, and thus performs worse.
In Gradient field, multiple drones can be attracted to the same packet, but only one will pick it up, and thus the other attracted drones will do an unnecessary effort, this while possibly having some packets in cargo.
Because it is important to respect the pick up time window, the drones will be more attracted to the packets to be picked up as the pick up time window advances, than to the warehouses where they have to deliver packets to be able to reduce their load. \\

Thus, the following hypotheses isn't confirmed:
\begin{itemize}
\item When looking at the average battery consumption per drone, CNET performs worse than CNET with negotiations.
\end{itemize}
And these hypotheses are confirmed:
\begin{itemize}
\item When looking at the average battery consumption per drone, Gradient field performs worse than CNET.
\item When looking at the average battery consumption per drone, Gradient field performs worse than CNET with negotiations.
\end{itemize}

See also section 2.4.

\subsection{Experiment 3: Comparing the number of tasks completed per unit of time}
The goal of the third experiment is to investigate how CNET, CNET with negotiations and Gradient Field compare to each other when looking at the number of tasks completed per unit of time when the packet arrival rate is varied.

\subsubsection{Setup}
The experiment is conducted in the following situation: 10 drones, 15 initial packets, 15 charging stations, 3 warehouses, a plane size of 1300m by 1300m, a drone capacity of 3 and every package has a pickup time window of 4000000 starting from the moment it is inserted into the system. The simulation is ran for 10 minutes. The packet arrival rate is varied between .002 and .011 in steps of .001. For every packet arrival rate we conducted 10 experiments each time using a different random seed. For each packet arrival rate, the average and standard deviation is calculated of the number of tasks completed per unit of time.

\paragraph{Variables}
Independent variables:
\begin{itemize}
    \item packet arrival rate
    \item random seed
\end{itemize}

Dependent variables: 
\begin{itemize}
    \item number of packets picked up per unit of time
\end{itemize}


\subsubsection{Results}

\input{./MasExperiments/tables/table13.tex}

\input{./MasExperiments/tables/table23.tex}

\input{./MasExperiments/tables/table33.tex}

\input{./MasExperiments/plots/plot3.tex}



\subsubsection{Analysis}
When the packet arrival rate is rather low. The drones are not pushed to their limits and the performance of the three algorithms is rather equal. It becomes more interesting when the packet arrival rate increases. Now the performance of each algorithm becomes more visible. Gradient Field performs slightly better than CNET and CNET with negotiations. This trend can already be observed starting from a low packet arrival rate. The explanation for this is similar to the one given in experiment 1. Therefore the following hypotheses can be confirmed:
\begin{itemize}
    \item When looking at the number of tasks completed per unit of time, CNET performs worse than Gradient Field.
    \item When looking at the number of tasks completed per unit of time, CNET with negotiations performs worse than Gradient Field.
\end{itemize}

We expected CNET to perform worse than CNET with negotiations, this is not what is seen in the results. When the packet arrival rate becomes larger CNET performs better than CNET with negotiations. The explanation is similar to the one given in experiment 1. This means that the following hypotheses is rejected:
\begin{itemize}
    \item When looking at the number of tasks completed per unit of time, CNET performs worse than CNET with negotiations.
\end{itemize}
See also section 2.4.

\section{Conclusion}
In this project we compared the scalability of CNET, CNET with negotiations and Gradient Field when applied in a pick-up and delivery problem. We researched the influence of adding more packets to the system and its impact on the performance of the three algorithms. We examined this performance by looking at the number of time window violations, the average battery consumption per drone, and the number of completed tasks per unit of time. We showed that Gradient Field outperforms CNET and CNET with negotiations when looking at the number of time window violations and the number of tasks completed per unit of time. CNET and CNET with negotiations outperforms Gradient Field when looking at the average battery consumption per drone. In contrast to what was expected, CNET performed slightly better than CNET with negotiations due to the overhead of computation and communication. 
Although Gradient Field has its limitations, it seems to be a better fit than CNET and CNET with negotiations for problems similar to the working example where the environment changes rapidly and fast response is needed. 
Finally, the efficiency of the negotiations in CNET with negotiations depends on the way the proposed deals are chosen, and on the way deals are accepted.
While for Gradient field, the strength of the attraction and repulsive forces of packets and warehouses are important factors.

\break
\section*{References}
Schut, M. (2007). Scientific handbook for simulation of collective intelligence.
\newline
\newline
Holvoet, T. Slides course H02H4a: Multi-Agent Systems.
\newline
\newline
Smith, R. (1980). The Contract Net Protocol: High-Level Communication and Control in a Distributed Problem Solver. \textit{IEEE Transactions on Computers C-29(12).}
\newline
\end{document}

